#!/usr/bin/env python
#  -*- coding: utf-8 -*-

"""
Run custom lrs run to create only illumination model.
This is done in parallel using pipeline_parallel. A preliminary run was done and determined the amount of RAM needed:
    xxx/SED_001.dat in 190.8 s (0.32 GB)
    xxx/SED_000.dat in 189.8 s (0.32 GB)
see mirisim_pool.py

"""

import glob
import os

import mirisim
from mirisim.lrssim.run_lrssim import run_lrssim
from mirisim.obssim.pointing import Pointing
from mirisim.skysim import *
import mirisim.config_parser as c
import pipeline_parallel
from mirisim.imsim.ima import get_lrs_v2v3_ref


def run_lrs_onesed(sed_file):
    """
    Run a single LRS custom simulation that produce only the illumination model

    :param str sed_file: absolute or relative path to an SED file (compatible with MIRISim standards)
    """

    # simulator_config = c.SimulatorConfig.from_default()
    simulator_config = c.SimulatorConfig.makeSimulator(
        take_webbPsf=False,
        include_refpix=True,
        include_poisson=False,
        include_readnoise=True,
        include_badpix=True,
        include_dark=True,
        include_flat=True,
        include_gain=True,
        include_nonlinearity=True,
        include_drifts=True,
        include_latency=False,
        cosmic_ray_mode='NONE')  # SOLAR_MIN, SOLAR_MAX, SOLAR_FLARE, NONE
    # local=True)

    config_path = 'LRS_SLITLESS'
    rel_obsdate = 0.0

    # Create a pointing
    ra_ref = 18.00  # deg
    dec_ref = 06.00  # deg
    pa = 0.0  # deg
    v2v3_off_commanded = [0.0, 0.0]
    v2v3_off_actual = [0.0, 0.0]

    v2v3_ref = get_lrs_v2v3_ref(config_path, simulator_config=simulator_config)
    
    # Create a pointing object.
    pointing = Pointing(ra_ref, dec_ref, pa, v2v3_off_commanded, v2v3_off_actual, v2v3_ref=v2v3_ref,
                        radec_pref=(ra_ref, dec_ref))

    background = Background(level='low', gradient=0, pa=0.0, centreFOV=(0., 0.))
    sed1 = ExternalSed(sedfile=sed_file)
    point1 = Point(Cen=(0., 0.))
    point1.set_SED(sed1)
    scene_config = c.SceneConfig.makeScene(loglevel=0, background=background, targets=[point1])

    scene = scenemaker(scene_config)

    illum_model = run_lrssim(scene, config_path, pointing, rel_obsdate, simulator_config)
    illum_model.meta['version'] = mirisim.__version__
    
    filename = os.path.basename(sed_file)
    basename, dummy = os.path.splitext(filename)
    out_file = f'{basename}.fits'

    illum_model.write(out_file)


def main():
    """
    multiprocessing can't be run in the main script or it will be run
    in every sub-process as well. Hence this main() function.
    """

    ##########################
    sed_dir = '/Users/gastaud/test_dap/cascade/simulation_LRS/test_generic9/POL2/output_del2/NoodleEclipse'
    # sed_dir = "/local/home/ccossou/PycharmProjects/exonoodle/test_notransit/LIGHTCURVES/55Cnc-e/Noodles_2020-03-18_1"
    ####################

    file_pattern = os.path.join(sed_dir, "SED*.dat")
    sed_files = sorted(glob.glob(file_pattern))

    # Debug
    # sed_files = sed_files[:2]

    if not sed_files:
        raise ValueError(f'No files found for: {file_pattern}')
    else:
        print(f'{len(sed_files)} files found ({file_pattern}).')

    ram = [1.] * len(sed_files)

    lvl1s = pipeline_parallel.ArgList(args=sed_files, ram=ram)

    pool = pipeline_parallel.ProcessPool(func=run_lrs_onesed, params=lvl1s)
    pool.run()


if __name__ == '__main__':
    main()
