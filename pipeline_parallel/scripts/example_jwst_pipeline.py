#!/usr/bin/env python
#  -*- coding: utf-8 -*-

"""
Run the JWST pipeline in parallel.

Now only lvl1 steps as a test
"""
import pipeline_parallel
pipeline_parallel.force_single_thread()

import os
import glob
import time

import logging

from jwst import datamodels
from jwst.pipeline import Detector1Pipeline, Image2Pipeline

LOG = logging.getLogger("main")

pipeline_parallel.init_log(log="pipeline.log", stdout_loglevel="INFO", file_loglevel="DEBUG")

input_fits_pattern = "test_*/det_images/*.fits"

def get_level1_images(file_regexp, exclude):
    """
    Retrieve all level1 FITS file in the current folder
    (and sub-directories)

    :param exclude: pattern forbidden in the FITS filename
    :type exclude: list(str)

    :param file_regexp: regexp pattern to be match to the files
    :type file_regexp: str

    :return: file_list. Each element being the relative (w.r.t CWD) or
        absolute path to a FITS file
    :rtype: list(str)
    """

    items = glob.glob(file_regexp)

    def good_item(x): return not (any(e in x for e in exclude))

    return list(filter(good_item, items))


def run_ima_single(rel_path):
    """
    Run everything we're capable of for a single level 1B file.


    :param rel_path: Level1 FITS relative or absolute path
    :type rel_path: str
    :return:
    :rtype:
    """

    LOG.info("Run lvl1 on {}".format(rel_path))

    (path, filename) = os.path.split(rel_path)

    os.chdir(path)

    # MIRI_SLOPER
    with datamodels.open(filename) as dm:
        dm.meta.wcsinfo.wcsaxes = 2
        dm_2_a = Detector1Pipeline.call(dm, save_results=True)

    # MIRI Image level 2B
    LOG.info("Run lvl2 on {}".format(rel_path))
    dm_2_b = Image2Pipeline.call(dm_2_a, save_results=True)

    return dm_2_b.meta.exposure.nints, dm_2_b.meta.exposure.ngroups


def test_run(filename):
    logging.info(filename)
    time.sleep(0.5)
    return 0


def main():
    """
    multiprocessing can't be run in the main script or it will be run 
    in every sub-process as well. Hence this main() function.
    """

    filenames = get_level1_images(file_regexp=input_fits_pattern, exclude=["_cal", "_i2d", "_rateints", "_rate"])

    lvl1s = pipeline_parallel.ArgList(args=filenames)

    pool = pipeline_parallel.ProcessPool(func=run_ima_single, params=lvl1s)
    pool.run()


def test():
    """
    Trying ProcessPool on a dummy example
    """
    filenames = get_level1_images(file_regexp=input_fits_pattern, exclude=["_cal", "_i2d", "_rateints", "_rate"])
    ram = [2.] * len(filenames)

    lvl1s = pipeline_parallel.ArgList(args=filenames, ram=ram)

    pool = pipeline_parallel.ProcessPool(func=test_run, params=lvl1s)
    pool.run()


if __name__ == '__main__':
    # main()
    test()

    # For class ProcessPool:
    # deal with fits file failure (no metadata or not a FITS file)
    # Deal with logging in sub-process and mask them to avoid spamming
