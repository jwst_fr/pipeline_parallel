#!/usr/bin/env python
#  -*- coding: utf-8 -*-

"""
Extract time and ram consumption from log file, retrieve metadata from the fits file and generate formated data
for RAM prediction analysis

This program is designed to be run in the directory where the pipeline log file is.
Both the log file, and the input .fits file must be available

Data will be appended in jwst_mem_profile.log and jwst_time_profile_cluster-r730-5.log in the CWD.
"""

import numpy as np
from astropy.io import fits
import re

log_filename = "pool.log"

time_filename = "jwst_time_profile_cluster-r730-5.log"
ram_filename = "jwst_mem_profile.log"

pattern = re.compile("^\t ([^ ]+) in ([0-9.]+) s \(([0-9.]+) GB\)\n$")

# Extract data from the log file
filenames = []
times = []
rams = []
with open(log_filename) as logfile:
    lines = logfile.readlines()

    extracted = []
    keep = False
    for line in lines:
        match = pattern.match(line)
        if match:
            (filename, time, ram) = match.groups()
            filenames.append(filename)
            times.append(time)
            rams.append(ram)

# Create the columns for the datafiles
timedatas = ""
ramdatas = ""
for (f, t, r) in zip(filenames, times, rams):
    with fits.open(f) as hdulist:
        metadata = hdulist[0].header
        frames = metadata["NGROUPS"]
        integrations = metadata["NINTS"]

    ramtmp = "{} {} {}\n".format(integrations, frames, r)
    timetmp = "{} {} {}\n".format(integrations, frames, t)

    timedatas += timetmp
    ramdatas += ramtmp

#Write the data files
with open(time_filename, "a") as f:
    f.write(timedatas)

with open(ram_filename, "a") as f:
    f.write(ramdatas)