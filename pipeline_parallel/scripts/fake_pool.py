#!/usr/bin/env python
#  -*- coding: utf-8 -*-

"""
Run a fake pool to test if it works
"""
import pipeline_parallel

# Must be before All critical package imports, especially including Numpy
pipeline_parallel.force_single_thread()

import time
import logging
import os

LOG = logging.getLogger("main")

pipeline_parallel.init_log(log="pipeline.log", stdout_loglevel="INFO", file_loglevel="DEBUG")

def my_function(arg):

    if arg == "f":
        raise ValueError("raise a failure for arg=f ; test of pool failure/restart")

    LOG.info("test_function in process with pid: {}".format(os.getpid()))
    LOG.info("MKL_THREADING_LAYER={}".format(os.environ["MKL_THREADING_LAYER"]))
    LOG.info("NUMEXPR_NUM_THREADS={}".format(os.environ["NUMEXPR_NUM_THREADS"]))
    LOG.info("OMP_NUM_THREADS={}".format(os.environ["OMP_NUM_THREADS"]))
    time.sleep(5)

def another_function(arg):
    LOG.info(f"another function called with {arg}")

def test_1cpu():
    """
    Trying ProcessPool on a dummy example
    """
    filenames = ["toto", "tata"]
    ram = [2.] * len(filenames)

    lvl1s = pipeline_parallel.ArgList(args=filenames, ram=ram)

    pool = pipeline_parallel.ProcessPool(func=my_function, params=lvl1s, cpu=1)
    pool.run()


def my_pool():
    arg_pools = ["a", "b", "c", "d", "e", "f"]

    # Estimate RAM needed by each run
    ram = [1.] * len(arg_pools)

    lvl1s = pipeline_parallel.ArgList(args=arg_pools, ram=ram)

    pool = pipeline_parallel.ProcessPool(func=my_function, params=lvl1s)
    pool.run()


if __name__ == '__main__':
    # print("Try with 1 CPU")
    # test_1cpu()

    print("Try with All CPUs")
    my_pool()


