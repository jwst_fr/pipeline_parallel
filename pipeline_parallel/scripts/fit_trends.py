#!/usr/bin/env python
#  -*- coding: utf-8 -*-

"""
Data hardcoded in frames_value, memory_value
"""

import numpy as np
from scipy.optimize import curve_fit
import matplotlib.pyplot as plt
import os


def fit_data(filename, func, title=None, actual=None, ylabel="RAM (GB)", display=False, ext="pdf"):
    """
    Display in the same plot the original data as points and the fitted curve as a line

    :param np.array frames_value: x axis
    :param np.array memory_value: original y data
    :param func func: fitting function object (e.g a*x + b) first argument must be x, all the remainings are constants
    :param list constants: fit results given by curve_fit
    :param list actual: [optional] (a, b) Constants for affine function y = a * x + b
    :param str ext: [optional] extension for saving figure (By default "pdf")

    """

    # Fit of data vs frames
    (nints, ngroups, ydata) = np.loadtxt(filename, skiprows=0, usecols=(0, 1, 2), dtype=float,
                                                unpack=True)
    xdata = nints * ngroups

    # Ordering values to prevent display bug where the dash line appear as a simple line on the plot
    indexes = np.argsort(xdata)
    xdata = xdata[indexes]
    ydata = ydata[indexes]

    # Fit
    constants, pcov = curve_fit(func, xdata, ydata)

    print("Fit result for {}:".format(ylabel))
    print(func_display.format(*constants))

    yfunc = func(xdata, *constants)

    # We prepare the plot
    fig = plt.figure()
    # We define a fake subplot that is in fact only the plot.
    plot1 = fig.add_subplot(1, 1, 1)

    plot1.grid(True)
    plot1.set_xlabel("Frames")
    plot1.set_ylabel(ylabel)
    plot1.plot(xdata, ydata, '.', label="Exposures (Simulations)")

    if actual:
        yfunc2 = func(xdata, *actual)
        plot1.plot(xdata, yfunc2, '-', label="Fit: "+func_display.format(*actual))
    else:
        plot1.plot(xdata, yfunc, '--', label="Fit: " + func_display.format(*constants))

    plot1.legend()

    if not title:
        title = filename

    fig.suptitle(title)

    (name, dummy) = os.path.splitext(filename)

    output = f"{name}.{ext}"

    fig.savefig(output, bbox_inches="tight", pad_inches=0.1)

    if display:
        plt.show()


def fit_subarray_data(filename, func, title=None, actual=None, ylabel="RAM (GB)", display=False):
    """
    Display in the same plot the original data as points and the fitted curve as a line
    Line format:
    nb_x_pixels nb_y_pixels value

    :param np.array frames_value: x axis
    :param np.array memory_value: original y data
    :param func func: fitting function object (e.g a*x + b) first argument must be x, all the remainings are constants
    :param list constants: fit results given by curve_fit
    :param list actual: [optional] (a, b) Constants for affine function y = a * x + b

    """

    # Fit of data vs frames
    (xpixels, ypixels, ydata) = np.loadtxt(filename, skiprows=0, usecols=(0, 1, 2), dtype=float,
                                                unpack=True)
    xdata = xpixels * ypixels

    # Ordering values to prevent display bug where the dash line appear as a simple line on the plot
    indexes = np.argsort(xdata)
    xdata = xdata[indexes]
    ydata = ydata[indexes]

    # Fit
    constants, pcov = curve_fit(func, xdata, ydata)

    print("Fit result (all):")
    print(func_display.format(*constants))

    yfunc = func(xdata, *constants)

    # We prepare the plot
    fig, ax = plt.subplots()
    fig.subplots_adjust(left=0.1, bottom=0.15, right=0.89, top=0.9, wspace=0.26, hspace=0.26)

    ax.grid(True)
    ax.set_ylabel(ylabel)
    ax.plot(xdata, ydata, '.', label="Exposures (Simulations)")
    ax.plot(xdata, yfunc, '--', label="Fit: "+func_display.format(*constants))

    if actual:
        yfunc2 = func(xdata, *actual)
        ax.plot(xdata, yfunc2, '-', label="Final: "+func_display.format(*actual))

    tick_position = [1032 * 1024, 512 * 512, 256 * 256, 136 * 128, 72 * 64]

    subarray_name = ["FULL", "BRIGHTSKY", "SUB256", "SUB128", "SUB64"]

    ax.set_xticks(tick_position)
    ax.set_xticklabels(subarray_name, rotation=45, horizontalalignment="right")

    ax.legend()

    if not title:
        title = filename

    fig.suptitle(title)

    (name, ext) = os.path.splitext(filename)

    output = "{}.pdf".format(name)

    fig.savefig(output, bbox_inches="tight", pad_inches=0.1)

    if display:
        plt.show()


def linear(x, a, b):
    return a * x + b


func_display = "{:.6g} * x + {:.6g}"

file_ext = "svg"
fit_data("jwst_mem_profile.log", linear, actual=[0.048, 4], ylabel="Pipeline 1.3.3 RAM (GB)", ext=file_ext)
fit_data("jwst_0.12.2_mem_profile.log", linear, actual=[0.04, 4.8], ylabel="Pipeline 7.2 RAM (GB)", ext=file_ext)
#fit_data("mirisim_mem_profile.log", linear, actual=[0.012, 4], ylabel="RAM (GB)")

#fit_subarray_data("subarray_7_2_mem_profile.log", linear, ylabel="RAM (GB)")
#fit_subarray_data("subarray_7_2_time_profile.log", linear, ylabel="Pipeline compute time (s)")

plt.show()
