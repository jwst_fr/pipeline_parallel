#!/usr/bin/env python
#  -*- coding: utf-8 -*-

"""
Example pool to run dozen of spectrum with the same MIRISim parameters. This usecase is typically what you want after
running exonoodle to produce a sampling of spectrums to reproduce a transit, and want to simulate a MIRI LRS TSO
observation.
"""

import pipeline_parallel

pipeline_parallel.force_single_thread()

# MIRISim
from mirisim import MiriSimulation
from mirisim.skysim import *
from mirisim.config_parser import *  # SimConfig, SimulatorConfig, SceneConfig
import glob
import subprocess
import logging
import os
import sys

LOG = logging.getLogger('main')

# Retrieve parameters from top script
# Hardcode values if you prefer but keep the abspath to avoid problems later
INPUT_DIR = os.path.abspath(sys.argv[1])
OUTPUT_DIR = os.path.abspath(sys.argv[2])


def run_single_mirisim(sed_file):
    """
    Run a single MIRISim simulation. The only thing that changes is the
    External SED used for the only point source of the scene.


    :param str sed_file: Filename for the SED file

    :return:
    :rtype:
    """

    # We want to access the file even after we change dir
    spectrum_path = os.path.abspath(sed_file)

    # Extract Spectrum ID from its filename (e.g. "SED_001.dat" -> "001")
    spectrum_id = os.path.splitext(os.path.basename(sed_file))[0].split("_")[1]

    new_dir = "simulation_{}".format(spectrum_id)
    sim_path = os.path.join(OUTPUT_DIR, new_dir)
    try:
        os.makedirs(sim_path)
        print(sim_path)
    except OSError:
        LOG.error("{} already exist, skipping simulation".format(new_dir))
        return 1

    os.chdir(sim_path)

    # We define the Simulation parameters for MIRISIM
    # sim_config = SimConfig.from_default()
    # scene_config = SceneConfig.from_default()
    # simulator_config = SimulatorConfig.from_default()
    # simulator_config = SimulatorConfig.makeSimulator(take_webbPsf=True)
    simulator_config = SimulatorConfig.makeSimulator(
        take_webbPsf=False,
        include_refpix=True,
        include_poisson=True,
        include_readnoise=True,
        include_badpix=True,
        include_dark=True,
        include_flat=True,
        include_gain=True,
        include_nonlinearity=True,
        include_drifts=False,
        include_latency=False,
        cosmic_ray_mode='NONE')  # SOLAR_MIN, SOLAR_MAX, SOLAR_FLARE, NONE

    # We prepare the scene we want to model
    background = Background(level='low', gradient=0., pa=0.0, centreFOV=(0., 0.))

    sed1 = ExternalSed(sedfile=spectrum_path)  # GENERATED BY [exoNoodle package]

    point1 = Point(Cen=(0., 0.))
    point1.set_SED(sed1)

    targets = [point1]

    scene_config = SceneConfig.makeScene(loglevel=1, background=background, targets=targets)

    sim_config = SimConfig.makeSim(name="Default Simulation", rel_obsdate=0.0, scene="scene.ini",
                                   POP='IMA', ConfigPath='LRS_SLITLESS', Dither=False, StartInd=1, NDither=2,
                                   DitherPat="lrs_recommended_dither.dat",
                                   filter="P750L", readDetect='SLITLESSPRISM', ima_mode='FAST', ima_exposures=1,
                                   ima_integrations=1, ima_frames=50,  # 65,
                                   disperser='SHORT', detector='SW', mrs_mode='FAST', mrs_exposures=1,
                                   mrs_integrations=1, mrs_frames=1)

    # We run the simulation
    # loglevel : "DEBUG", "INFO", "WARNING", "ERROR", "CRITICAL"
    ms = MiriSimulation(sim_config=sim_config, scene_config=scene_config,
                        simulator_config=simulator_config, loglevel='WARNING', local=True)

    ms.run()
    # ms.path_out = do_nothing()

    # move sublayer into parent directory
    command = "mv {} .".format(os.path.join(ms.path_out, "*"))
    subprocess.call(command, shell=True)  # Don't work with the list of args, don't ask me why

    # This command seem to create problems with mirisim.log still existing there.
    # I guess the timing is sometimes wrong, and stdout is still flushing to file after MIRISim finished the simulation
    # Delete temporary simulation folder created by MIRISim after we moved everything to parent directory
    # os.rmdir(ms.path_out)

    # Go back to original dir
    # Absolute path to avoid problems
    os.chdir(OUTPUT_DIR)


def do_nothing():
    """
    Used to test the pool before the real one

    :return:
    :rtype:
    """
    import datetime

    LOG.info("test run")

    # Create a fake folder with files in it
    new_dir = "{}_mirisim".format(datetime.datetime.now().strftime("%Y%m%d_%H%M%S"))
    os.mkdir(new_dir)  # 20180808_165027_mirisim

    fileobj = open(os.path.join(new_dir, "fichier.ini"), 'w')
    fileobj.write("42\n\n")
    fileobj.close()

    return new_dir


def run_pool():
    """
    In the CWD must exist a folder (see exonoodle_simulation variable) that contains several External SED files


    """

    extra_config = {"loggers":
        {

            "paramiko":
                {
                    "level": "WARNING",
                },

            "matplotlib":
                {
                    "level": "WARNING",
                },

            "astropy":
                {
                    "level": "WARNING",
                },
            "stpipe":
                {
                    "level": "WARNING",
                },
            "stdatamodels":
                {
                    "level": "WARNING",
                },
        }, }

    sed_files = glob.glob(os.path.join(os.path.abspath(INPUT_DIR), "SED_*.dat"))
    sed_files.sort()

    ram = [2.] * len(sed_files)

    os.chdir(INPUT_DIR)

    pipeline_parallel.init_log(log="mirisim.log", stdout_loglevel="INFO", file_loglevel="DEBUG",
                               extra_config=extra_config)

    lvl1s = pipeline_parallel.ArgList(args=sed_files, ram=ram)
    pool = pipeline_parallel.ProcessPool(func=run_single_mirisim, params=lvl1s, delay=0.)
    pool.run()


if __name__ == '__main__':
    run_pool()
