#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Depending on the server CPU and memory, predict the runtime.
# Show how server performances affect runtime for CARs

import numpy as np
from pipeline_parallel.fake_server import FakeServer

import matplotlib.pyplot as plt
from matplotlib.ticker import FormatStrFormatter

# Data for MIRI_11, hardcoded, but obtained using the apt_parser package.
mem_volume = [3.9, 3.9, 3.9, 3.9, 3.9, 3.9, 3.9, 3.9, 3.9, 3.9, 3.9, 3.9, 4.4, 4.4, 4.4, 4.4, 2.9, 2.9, 2.9, 2.9, 0.2,
              0.2, 0.2, 0.2, 1.3, 1.3, 1.3, 1.3, 3.9, 3.9, 3.9, 3.9, 3.9, 3.9, 3.9, 3.9, 3.9, 3.9, 3.9, 3.9, 3.4, 3.4,
              3.4, 3.4, 3.9, 3.9, 3.9, 3.9, 2.8, 2.8, 2.8, 2.8, 1.1, 1.1, 1.1, 1.1, 0.2, 0.2, 0.2, 0.2, 3.9, 3.9, 3.9,
              3.9, 3.9, 3.9, 3.9, 3.9, 3.9, 3.9, 3.9, 3.9, 4.4, 4.4, 4.4, 4.4, 2.9, 2.9, 2.9, 2.9, 0.2, 0.2, 0.2, 0.2,
              1.3, 1.3, 1.3, 1.3, 3.9, 3.9, 3.9, 3.9, 3.9, 3.9, 3.9, 3.9, 3.9, 3.9, 3.9, 3.9, 3.4, 3.4, 3.4, 3.4, 3.9,
              3.9, 3.9, 3.9, 2.8, 2.8, 2.8, 2.8, 1.1, 1.1, 1.1, 1.1, 0.2, 0.2, 0.2, 0.2, 110.4, 110.4, 110.4, 110.4,
              38.4, 38.4, 38.4, 56.4, 56.4, 6.6, 6.6, 6.6]

time_volume = [0.06, 0.06, 0.06, 0.06, 0.06, 0.06, 0.06, 0.06, 0.06, 0.06, 0.06, 0.06, 0.07, 0.07, 0.07, 0.07, 0.03,
               0.03, 0.03, 0.03, 0.01, 0.01, 0.01, 0.01, 0.03, 0.03, 0.03, 0.03, 0.06, 0.06, 0.06, 0.06, 0.06, 0.06,
               0.06, 0.06, 0.06, 0.06, 0.06, 0.06, 0.04, 0.04, 0.04, 0.04, 0.06, 0.06, 0.06, 0.06, 0.02, 0.02, 0.02,
               0.02, 0.02, 0.02, 0.02, 0.02, 0.01, 0.01, 0.01, 0.01, 0.06, 0.06, 0.06, 0.06, 0.06, 0.06, 0.06, 0.06,
               0.06, 0.06, 0.06, 0.06, 0.07, 0.07, 0.07, 0.07, 0.03, 0.03, 0.03, 0.03, 0.01, 0.01, 0.01, 0.01, 0.03,
               0.03, 0.03, 0.03, 0.06, 0.06, 0.06, 0.06, 0.06, 0.06, 0.06, 0.06, 0.06, 0.06, 0.06, 0.06, 0.04, 0.04,
               0.04, 0.04, 0.06, 0.06, 0.06, 0.06, 0.02, 0.02, 0.02, 0.02, 0.02, 0.02, 0.02, 0.02, 0.01, 0.01, 0.01,
               0.01, 3.31, 3.31, 3.31, 3.31, 1.11, 1.11, 1.11, 1.66, 1.66, 0.14, 0.14, 0.14]



def optimize_server(rams_input, times_input, ylabel, title=None):
    server_cpus = np.arange(1,50)
    server_rams = [16, 32, 64, 128, 256, 512, 1024]  # GB
    runtimes = {}

    prop_cycle = plt.rcParams['axes.prop_cycle']
    colors = prop_cycle.by_key()['color']

    for ram in server_rams:
        runtime = []

        for cpu in server_cpus:
            s = FakeServer(rams_input, times_input, total_ram=ram, total_cpu=cpu)
            r = s.predict_parallel_time()
            runtime.append(r)

        # NaN if not enough ram
        if np.isfinite(r).all():
            runtimes[ram] = runtime


    fig, ax = plt.subplots()
    runtime_keys = list(runtimes.keys())
    runtime_keys.sort()
    for i, ram in enumerate(server_rams):
        if ram in runtimes.keys():
            ax.semilogy(server_cpus, runtimes[ram], color=colors[i], label="{} GB".format(ram))
    ax.set_ylabel(ylabel)
    ax.set_xlabel("Number of CPU")
    ax.set_axisbelow(True)
    ax.xaxis.grid(True, which='major', color='#aaaaaa', linestyle='--')
    ax.yaxis.grid(True, which='major', color='#aaaaaa', linestyle='--')
    ax.yaxis.grid(True, which='minor', color='#cccccc', linestyle=':')
    ax.legend()
    ax.set_xlim(xmin=0)
    ax.set_ylim(ymin=1)
    myyfmt = FormatStrFormatter("%.3g")
    ax.yaxis.set_major_formatter(myyfmt)
    ax.yaxis.set_minor_formatter(myyfmt)
    ax.tick_params(axis='both', which='minor', labelsize=7)

    if title:
        fig.suptitle(title)

    return fig


car = "MIRI-011"
miri_11_runtime = optimize_server(mem_volume, time_volume, ylabel="Total runtime [h]")
miri_11_runtime.savefig("server_time_{}.pdf".format(car), bbox_inches="tight", pad_inches=0.1)


plt.show()