#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""

Test of the config parser refactor and skysim classes

@author: Christophe Cossou

"""

import pytest
import logging
import numpy as np

import pipeline_parallel

# Disable everything below the specified level (NOTSET, DEBUG, INFO, WARNING, ERROR, CRITICAL)
logging.disable(logging.WARNING)

testdata = [
    (1.5, 2, ((None, None), (None, None))),
    (2.5, 1, ((("file1.fits",), 2.), (None, None))),
    (10., 0, ((("file2.fits",), 3.), (("file1.fits",), 2.)))
]


@pytest.mark.parametrize("max_weight,remains,outs", testdata)
def test_getnextfile(max_weight, remains, outs):
    """
    test ArgList get_next_item member function

    :param max_weight: Upper limit for RAM allowed when retrieving the next file (in GB)
    :type max_weight: float
    :param remains: Number of files remaining in the ArgList at the end of the test
    :type remains: int
    :param outs: list of output for the .get_next_item() calls
    :type outs: list(tuple(str,float))
    """

    files = [("file1.fits",), ("file2.fits",)]
    rams = [2., 3.]

    ref_files = files.copy()
    ref_rams = rams.copy()
    ref_files.reverse()
    ref_rams.reverse()

    objs = pipeline_parallel.ArgList(files, rams)

    # Files must be ordered from biggest to lowest RAM
    np.testing.assert_equal(objs.args, ref_files)
    np.testing.assert_equal(objs.weight, ref_rams)
    assert (objs.size == len(files)), "Number of files different from input number"

    for (ref_name, ref_ram) in outs:
        (name1, ram1) = objs.get_next_item(max_weight)
        assert (name1 == ref_name), "Name doesn't match input ({} != {})".format(name1, ref_name)
        assert (ram1 == ref_ram), "Memory doesn't match input ({} != {})".format(ram1, ref_ram)

    # Remaining files in the ArgList
    assert (len(objs.args) == remains), \
        "Remaining file in list is not what's expected ({} != {})".format(len(objs.args), remains)

def test_empty_list():
    """
    Test ArgList when given an empty list (must crash)

    :return:
    :rtype:
    """

    with pytest.raises(ValueError):
        pipeline_parallel.ArgList(args=[])
