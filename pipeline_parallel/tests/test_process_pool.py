#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""

Test of the config parser refactor and skysim classes

@author: Christophe Cossou

"""

import os
import pytest
import logging
import pipeline_parallel

# Disable everything below the specified level (NOTSET, DEBUG, INFO, WARNING, ERROR, CRITICAL)
logging.disable(logging.WARNING)


def test_process_pool_init():
    """
    test ArgList get_next_item member function

    """

    files = ["file1.fits", "file2.fits"]
    rams = [0.5, 1.]

    objs = pipeline_parallel.ArgList(files, rams)

    pool = pipeline_parallel.ProcessPool(func=print, params=objs, delay=0)

    assert (pool.is_running() is False), "Wrong pool status: {}".format(pool.is_running())
    assert (pool.tstart is None), "tstart is set ({}) while it shouldn't".format(pool.tstart)
    assert (pool.tstop is None), "tstop is set ({}) while it shouldn't".format(pool.tstop)

    code = pool.run()

    # Return code must be 0 if it's ok (and it should)
    assert (code == 0), "Return code is not 0"
    assert (pool.is_running() is False), "Wrong pool status: {}".format(pool.is_running())


def test_process_pool_hardware_limits():
    """
    Test ProcessPool hardware limits
    """

    files = ["file1.fits", "file2.fits"]
    rams = [0.5, 1]

    objs = pipeline_parallel.ArgList(files, rams)

    # get limits from first dummy instance
    pool = pipeline_parallel.ProcessPool(func=print, params=objs, delay=0)
    cpu_count = pool.max_cpu
    ram_count = pool.max_ram / (1. - pool.RAM_SAFETY)

    ram_over = ram_count + 1.
    cpu_over = cpu_count + 1
    with pytest.raises(ValueError):
        pipeline_parallel.ProcessPool(func=print, params=objs, ram=ram_over, delay=0)

    with pytest.raises(ValueError):
        pipeline_parallel.ProcessPool(func=print, params=objs, cpu=cpu_over, delay=0)

    with pytest.raises(ValueError):
        pipeline_parallel.ProcessPool(func=print, params=objs, ram=0.5, delay=0)


def test_restart():
    """
    Test ProcessPool restarts
    """

    files = ["file1.fits", "file2.fits"]
    rams = [0.4, 0.6]
    objs = pipeline_parallel.ArgList(files, rams)

    r_files = [("file3.fits",), ("file4.fits",)]
    r_rams = [0.7, 0.5]
    r_objs = pipeline_parallel.ArgList(r_files, r_rams)

    # Create fake restart file
    pool = pipeline_parallel.ProcessPool(func=print, params=r_objs, delay=0)
    pool._ProcessPool__write_restart_file()

    pool = pipeline_parallel.ProcessPool(func=print, params=objs, delay=0)

    # Ensure the new values are ignored and the one of the restart files are used instead.
    assert pool.params.args == r_files
    assert pool.params.weight == r_rams

    # Try & remove restart file
    os.remove(pool.RESTART_FILENAME)
